import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NumGen from './NumGen';

class App extends Component {
  // Creates a list of 10,000 unique elements
  // comprised of number from 1 to 10000
  buildNumList() {
    let list = [];
    for(var i = 1;i<=10000;i++) {
      list.push(i);
    }
    return list;
  }

  // Randomly shuffle an array
  // This is the Fisher-Yates algorithm for shuffling an array
  shuffleArray(arrayShuffle) {
    let currentIndex = arrayShuffle.length, tempValue, randomIndex;

    while(currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        tempValue = arrayShuffle[currentIndex];
        arrayShuffle[currentIndex] = arrayShuffle[randomIndex];
        arrayShuffle[randomIndex] = tempValue;
    }
    return arrayShuffle;
  }

  render() {
    let arra = this.shuffleArray(this.buildNumList());
    return (
      <div className="App">
        <NumGen shuffleArray={arra}/>
      </div>
    );
  }
}

export default App;
