import React from 'react';
import {WindowScroller, AutoSizer, List} from 'react-virtualized';

// This class displays the randomized list
// It only renders list elements that fit in the 
// viewport using the `react-virtualized` library
class NumGen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    };

    renderRow = ({index, key, style}) => {
        return(
            <div style={style} key={key}>
                {this.props.shuffleArray[index]}
            </div>
        )
    }

    render() {

        return(
            <div style={{height: '100%', overflowY: 'scroll'}}>
                NumGen
                <WindowScroller>
                    {({height, scrollTop}) => (
                        <AutoSizer disableHeight>
                            {({width}) => (
                                <List
                                    autoHeight
                                    height={height}
                                    width={width}
                                    scrollTop={scrollTop}
                                    rowHeight={18}
                                    rowRenderer={this.renderRow}
                                    rowCount={this.props.shuffleArray.length}
                                    overscanRowCount={5}
                                />
                            )}
                        </AutoSizer>
                    )}
                </WindowScroller>
            </div>
        );
    }
}

export default NumGen;